#!/usr/bin/env python

"""
Finding saddle points of lattice action via gradient descent.
"""

import argparse
import time

import jax
import jax.numpy as jnp
import jax.random as jr
import optax

jax.config.update("jax_enable_x64", True)

from models import resonance, well

if __name__ == '__main__':
    # Specify to use CPUs
    jax.config.update('jax_platform_name', 'cpu')

    parser = argparse.ArgumentParser(
            description="Finding saddle points in QM",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@')
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('-lr', '--learningrate', type=float, default=1e-3, help='Learning rate')
    parser.add_argument('-ns', '--samples', type=int, default=100, help='number of trials')
    parser.add_argument('-nd', '--descent', type=int, default=1e6, help='maximum number of descent steps')
    parser.add_argument('-o', '--optimizer', choices=['adam', 'sgd', 'yogi'], default='adam', help='optimizer to use')
    parser.add_argument('-r', '--radius', type=float, default=5.0, help='Gaussian radius for initial x')
    parser.add_argument('-s', '--seed', type=int, default=None, help='random seed (defaults to time)')
    parser.add_argument('-t', '--threshold', type=float, default=1e-6, help='maximal ds to achieve')

    args = parser.parse_args()
    args.descent = int(args.descent)

    with open(args.model, 'rb') as f:
        model = eval(f.read())

    seed = args.seed
    if seed is None:
        seed = time.time_ns()
    key = jr.PRNGKey(seed)

    Dim = model.D
    R = args.radius
    
    action_grad = jax.grad(model.action, holomorphic=True)

    @jax.value_and_grad
    def loss(x):
        z = x[:Dim] + 1j*x[Dim:]
        dsdx = action_grad(z)
        return jnp.mean(jnp.abs(dsdx)**2)

    def step1(x, opt_state):
        ds, grad = loss(x)
        updates, opt_state = opt.update(grad, opt_state)
        x = optax.apply_updates(x, updates)
        return x, opt_state

    @jax.jit
    def step(x, opt_state, N=10000):
        def body(n, d):
            return step1(*d)
        x, opt_state = jax.lax.fori_loop(0, N, body, (x, opt_state))
        ds, _ = loss(x)
        return x, opt_state, ds

    opt = getattr(optax,args.optimizer)(args.learningrate)

    for _ in range(args.samples):
        key, samplekey = jr.split(key, 2)
        x = jnp.concatenate((jr.normal(samplekey, shape=(Dim,)) * R, jnp.zeros(Dim)))
        opt_state = opt.init(x)
        itr = 0
        for k in range(args.descent):
            x, opt_state, ds = step(x, opt_state)
            itr += 1
            print(ds)
            if ds < args.threshold:
                break

        s = model.action(x[:Dim] + 1j*x[Dim:]).real
        sds = f'{itr} {s:.5e} {ds:.5e}'
        xstr = ' '.join([str('{:.5f}'.format(i)) for i in x])
        print(sds)
        print(*x[:Dim])
        print(*x[Dim:])

