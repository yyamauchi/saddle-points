from dataclasses import dataclass

import jax.numpy as jnp
import numpy as np

@dataclass
class Model:
    m: float
    k: float
    g: float
    nt: int
    dt: float
    x0: float
    p0: float
    dx: float

    def __post_init__(self):
        self.T = jnp.concatenate([-1j*jnp.ones(self.nt), 1j*jnp.ones(self.nt)]) * self.dt
        self.D = self.nt * 2 + 1

    def initstate(self, x):
        return (x-self.x0)**2/2/self.dx**2 - 1j * self.p0 * x 

    def finstate(self, x):
        return (x-self.x0)**2/2/self.dx**2 + 1j * self.p0 * x

    def action_kinetic(self, x):
        kin = jnp.sum((x[1:] - x[:-1])**2/self.T)/2/self.m
        return kin

    def V(self, x):
        return self.k * x**2 + self.g * x**3

    def action_potential(self, x):
        pot = jnp.sum(self.V(x[:-1])*self.T/2. + self.V(x[1:])*self.T/2.)
        return pot

    def action(self, x):
        return self.action_kinetic(x) + self.action_potential(x) + self.initstate(x[0]) + self.finstate(x[-1])

