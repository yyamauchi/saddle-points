# Finding saddle points of lattice actions

This repository contains a set of codes for searching for the saddle points in lattice actions.


## Getting started
Installation of python libraries:
```
python -m venv env
. env/bin/activate
pip install --upgrade pip
pip install numpy
pip install jax
pip install jaxlib
pip install optax
```

## Models
Lattice actions are stored in the scripts in `model` directory. The directory also contains sample parameters' files.

## Findign saddle points
To find saddle points for an action, pick a model and run
```
./saddles.py paramsfile
```
Pass in `-h` for various options.
